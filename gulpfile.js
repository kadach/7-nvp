var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglifyjs'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();

// gulp.task('auto-pre', function () {
//     return gulp.src('app/css/*.css')
//         .pipe(autoprefixer({
//             browsers: ['last 7 versions'],
//             cascade: false
//         }))
//         .pipe(gulp.dest('app/css'));
// });

gulp.task('sass', function () {
    return gulp.src('dist/sass/**/*.scss')
    // {outputStyle: 'compressed'}
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app/css/'))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function () {
    return gulp.src([
        'dist/js/*.js'
    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'))
        .pipe(browserSync.stream());
});
// gulp.task('browser-sync', function() {
//     browserSync.init({
//         server: {
//             baseDir: "app/index.html"
//         }
//     });
// });
// ,'auto-pre' gulp.watch('app/css/*.css',['auto-pre'])
gulp.task('watch', ['sass', 'scripts'], function () {
    // browserSync.reload();
    browserSync.init({
        server: "app"
    });
    gulp.watch('dist/sass/**/*.scss', ['sass']);
    gulp.watch('dist/js/*.js', ['scripts']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['watch']);