(function ($) {
    var slider = $('.slider');
    slider.slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        vertical: true,
        centerMode: true,
        centerPadding: '0px',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    vertical: false
                }
            }
        ]
    });
    var allSlideInfo = $('.one-slide-info');
    slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        allSlideInfo.removeClass('see');
        setTimeout(function () {
            allSlideInfo.removeClass('life')
        }, 500);
    });
    slider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
        var centerSlide = $('.slide-info-' + ($('.slick-center')).attr('data-slade'))
        centerSlide.addClass('life');
        setTimeout(function () {
            centerSlide.addClass('see')
        }, 30);
    });

//    reviews slider
    $('.rev-slider-wrap').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true
    });


    var tellBtn = $('[data-tell]');
    var userTell = /^((\d|\+\d)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6,13}$/;
    tellBtn.click(function () {
        var inVal = $('.input-wrap input').val();
        alert('Запрос отправлен');
        if (userTell.test(inVal) && inVal.length > 6) {
            var data1 = {tell: inVal};
            $.ajax({
                // url: "../sender.php",
                type: "POST",
                data: data1,
                success: function () {
                   alert('Ваше сообшение отправленно')
                }
            });
        } else {
            alert('error')
        }
    })
})(jQuery);
